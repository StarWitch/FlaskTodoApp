# TodoExampleApp

A short exercise to teach myself Flask, SQLAlchemy, and WTFlask Forms.

It was developed primarily on Python 3.7 and Flask 1.0.2.

## Running

```
cd TodoExampleApp
python3 -m venv venv
. venv/bin/activate
pip3 install -r requirements.txt
FLASK_SEED_DB=1 flask run
```

And then navigate to http://127.0.0.1:5000/ .

## Testing

```
cd TodoExampleApp
python3 -m pytest
```


