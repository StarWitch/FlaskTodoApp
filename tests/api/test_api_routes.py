from datetime import datetime
import json
def test_basic_load(test_client, init_db):
    response = test_client.get('/api/tasks/')
    assert response.status_code == 200
    assert b'tasks' in response.data

def test_add(test_client, init_db):
    response = test_client.post('/api/tasks/', data=json.dumps(dict(name="Test Task 1", owner_id=2, priority_id=3, due_date='2019-04-30')), follow_redirects=True, content_type='application/json')
    assert response.status_code == 201
    assert b'Test Task 1' in response.data
    assert b'"priority_id": 3' in response.data
    assert b'"owner_id": 2' in response.data
    assert b'2019-04-30' in response.data

def test_delete(test_client, init_db):
    response = test_client.get('/api/tasks/')
    assert response.status_code == 200
    assert b'Test 1' in response.data

    response = test_client.delete('/api/tasks/1', follow_redirects=True, content_type='application/json')
    assert b'Test 1' not in response.data

def test_edit(test_client, init_db):
    response = test_client.get('/api/tasks/')
    assert response.status_code == 200
    assert b'Test 2' in response.data

    response = test_client.put('/api/tasks/2', data=json.dumps(dict(do_edit=1, id=2, name="Modified Task 1", owner_id=2, priority_id=3, due_date='2019-04-30')), follow_redirects=True, content_type='application/json')
    assert b'Modified Task 1' in response.data

