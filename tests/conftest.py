import pytest
from datetime import datetime
from todo import create_app, db
from todo.models import Task, Priority, Owner

@pytest.fixture(scope='module')
def test_client():
    print("Starting tests...")
    flask_app = create_app('test.cfg')
    test_client = flask_app.test_client()
    context = flask_app.app_context()
    context.push()
    yield test_client
    context.pop()

@pytest.fixture(scope='module')
def init_db():
    db.create_all()

    # categories

    blank = Priority(name="Uncategorized", colour=False)
    blue = Priority(name="Low", colour="#66b3ff")
    red = Priority(name="High", colour="#ff6666")
    green = Priority(name="Medium", colour="#66ff66")

    db.session.add(blank)
    db.session.add(red)
    db.session.add(blue)
    db.session.add(green)

    # users

    unassigned = Owner(name="Unassigned")
    test_owner = Owner(name="Test Owner")
    db.session.add(unassigned)
    db.session.add(test_owner)

    # basic tasks
    task_1 = Task(name="Test 1", priority_id=1, owner_id=1, due_date=datetime.strptime('2019-04-30', '%Y-%m-%d'))
    task_2 = Task(name="Test 2", priority_id=1, owner_id=1, due_date=datetime.strptime('2019-04-30', '%Y-%m-%d'))

    db.session.add(task_1)
    db.session.add(task_2)

    # commit!
    db.session.commit()

    yield db

    db.drop_all()

@pytest.fixture(scope='module')
def new_task():
    test_task = Task(name="Test Task", priority_id=1, owner_id=1, due_date=datetime.strptime('2019-04-30', '%Y-%m-%d'))
    return test_task

@pytest.fixture(scope='module')
def new_owner():
    test_owner = Owner(name="Test Owner")
    return test_owner

@pytest.fixture(scope='module')
def new_priority():
    test_priority = Priority(name="Test Priority", colour="#000000")
    return test_priority
