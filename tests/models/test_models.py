def test_task_model(new_task):
    assert new_task.name == "Test Task"
    assert new_task.priority_id == 1
    assert new_task.owner_id == 1

def test_priority_model(new_priority):
    assert new_priority.name == "Test Priority"
    assert new_priority.colour == "#000000"

def test_owner_model(new_owner):
    assert new_owner.name == "Test Owner"
