from datetime import datetime

def test_load(test_client, init_db):
    response = test_client.get('/frontend/')
    assert response.status_code == 200
    assert b'ToDo Example App!' in response.data

def test_add(test_client, init_db):
    response = test_client.post('/frontend/add', data=dict(name="Test Task 1", owner=2, priority=3, due_date='2019-04-30'), follow_redirects=True)
    assert response.status_code == 200
    assert b'Test Task 1' in response.data
    assert b'Unassigned' in response.data
    assert b'High' in response.data
    assert b'2019-04-30' in response.data

def test_delete(test_client, init_db):
    response = test_client.get('/frontend/')
    assert response.status_code == 200
    assert b'Test 1' in response.data

    response = test_client.post('/frontend/delete', data=dict(id=1), follow_redirects=True)
    assert b'Test 1' not in response.data

def test_edit(test_client, init_db):
    response = test_client.get('/frontend/')
    assert response.status_code == 200
    assert b'Test 2' in response.data

    response = test_client.post('/frontend/edit', data=dict(do_edit=1, id=2, name="Modified Task 1", owner=2, priority=3, due_date='2019-04-30'), follow_redirects=True)
    assert b'Modified Task 1' in response.data
