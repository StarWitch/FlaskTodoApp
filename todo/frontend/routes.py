from flask import flash, render_template, request, url_for, redirect
from sqlalchemy import inspect
from datetime import datetime

from todo.models import Task, Priority, Owner
from todo.frontend.forms import AddTaskForm, EditTaskForm
from todo.frontend import frontend_blueprint

# primary index that routes most things
@frontend_blueprint.route('/', methods=['GET'])
@frontend_blueprint.route('/edit/<int:edit>', methods=['GET', 'POST'])
@frontend_blueprint.route('/sort/<string:sort>/<int:reverse>', methods=['GET'])
def index(edit=None, sort='id', reverse=False):
    # not running things at-scale...so just pull everything
    tasks = Task.query.all()
    priorities = Priority.query.all()
    owners = Owner.query.all()
    # Flask doesn't like it if these are assigned in the Form class itself
    # so, do it here instead once the app is fully initialized
    form = AddTaskForm()
    form.priority.choices = ((priority.id, priority.name) for priority in priorities)
    form.owner.choices = ((owner.id, owner.name) for owner in owners)

    # if an /edit/<id> has been specified, load the edit form for that row
    edit_form = None
    if edit:
        edit = int(edit) # convert to int to make Jinja happy
        edit_task = Task.query.get(edit)
        if not edit_task:
            flash('ID not found', 'error')
            return redirect(url_for('frontend.index'))
        edit_form = EditTaskForm() # pre-populate with old details
        edit_form.name.default = edit_task.name
        edit_form.priority.choices = ((priority.id, priority.name) for priority in priorities)
        edit_form.owner.choices = ((owner.id, owner.name) for owner in owners)
        edit_form.priority.default = edit_task.priority.id
        edit_form.owner.default = edit_task.owner.id
        edit_form.due_date.default = edit_task.due_date
        edit_form.process()

    return render_template('main.html',
            tasks = tasks, priorities = priorities, form = form,
            edit_form = edit_form, edit = edit, sort = sort, reverse = int(reverse))

# add function
@frontend_blueprint.route('/add', methods = ['POST'])
def add():
    if not request.form:
        flash(u'Must specify task to add', 'warning')
        return redirect(url_for('frontend.index'))

    name = request.form['name']
    due_date = datetime.strptime(request.form['due_date'], "%Y-%m-%d")
    priority = Priority.query.get(request.form['priority'])
    owner = Owner.query.get(request.form['owner'])
    task = Task(name=name, due_date=due_date, priority=priority, owner=owner)
    task.save()

    flash(u'Successfully added task {0} with ID {1}'.format(task.name, task.id), 'info')
    return redirect(url_for('frontend.index'))


# edit function
@frontend_blueprint.route('/edit', methods = ['POST'])
def edit():
    if not 'do_edit' in request.form or not 'id' in request.form:
        flash(u'Must specify task to modify', 'warning')
        return redirect(url_for('frontend.index'))
    task_to_edit = Task.query.get(request.form['id'])
    task_to_edit.name = request.form['name']
    task_to_edit.due_date = datetime.strptime(request.form['due_date'], "%Y-%m-%d")
    task_to_edit.priority = Priority.query.get(request.form['priority'])
    task_to_edit.owner = Owner.query.get(request.form['owner'])
    task_to_edit.save()

    flash(u'Successfully modified task ID: {0}'.format(task_to_edit.id), 'info')
    return redirect(url_for('frontend.index'))

# delete function
@frontend_blueprint.route('/delete', methods = ['POST'])
def delete():
    if not request.form:

        flash(u'Must specify task to delete', 'warning')
        return redirect(url_for('frontend.index'))

    id = request.form['id']
    task_to_delete = Task.query.get(id)
    task_to_delete.delete()

    flash(u'Successfully deleted task ID: {0}'.format(id), 'info')
    return redirect(url_for('frontend.index'))

