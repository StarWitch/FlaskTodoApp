from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField, DateField
from wtforms.validators import DataRequired
from wtforms.fields.html5 import DateField

class AddTaskForm(FlaskForm):
    # form validator for adding a task
    # choices for owner/priority are specified in the routes file
    submit = SubmitField(u'Add Task')
    name = StringField(u'name', validators=[DataRequired()])
    due_date = DateField(u'due_date', validators=[DataRequired()], format='%Y-%m-%d')
    priority = SelectField(u'priority', validators=[DataRequired()], choices=[])
    owner = SelectField(u'owner', validators=[DataRequired()], choices=[])

class EditTaskForm(FlaskForm):
    # form validator for adding a task
    # choices for owner/priority are specified in the routes file
    submit = SubmitField(u'Save Task')
    name = StringField(u'name', validators=[DataRequired()])
    due_date = DateField(u'due_date', validators=[DataRequired()], format='%Y-%m-%d')
    priority = SelectField(u'pPriority', validators=[DataRequired()], choices=[])
    owner = SelectField(u'owner', validators=[DataRequired()], choices=[])
