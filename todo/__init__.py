import os
from time import sleep
from datetime import datetime
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

db = SQLAlchemy()

def create_app(conf_file=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile(conf_file)
    migrate = Migrate(app, db)

    db.init_app(app)

    from todo.frontend import frontend_blueprint
    from todo.api import api_blueprint
    app.register_blueprint(frontend_blueprint, url_prefix='/frontend')
    app.register_blueprint(api_blueprint, url_prefix='/api/tasks')

    return app

def seed_db(app):
    from todo.models import Task, Priority, Owner
    print("Creating DB tables (if they don't already exist)")
    with app.app_context():
        print("WARNING!!! This will wipe out your DB!")
        print("Ctrl+C to stop this! And unset 'FLASK_SEED_DB' the next time you run it!")
        sleep(5)
        db.drop_all()
        print("DB reset")
        db.create_all()

        # categories

        blank = Priority(name="Uncategorized", colour="#FFFFFF")
        blue = Priority(name="Low", colour="#66b3ff")
        green = Priority(name="Medium", colour="#66ff66")
        red = Priority(name="High", colour="#ff6666")

        db.session.add(blank)
        db.session.add(blue)
        db.session.add(green)
        db.session.add(red)

        # users

        unassigned = Owner(name="Unassigned")
        test_owner = Owner(name="Test Owner")
        test_owner_2 = Owner(name="Test Owner Two")
        db.session.add(unassigned)
        db.session.add(test_owner)
        db.session.add(test_owner_2)

        # basic tasks
        task_1 = Task(name="Test 1", priority_id=1, owner_id=1, due_date=datetime.strptime('2019-04-30', '%Y-%m-%d'))
        task_2 = Task(name="Test 2", priority_id=2, owner_id=2, due_date=datetime.strptime('2019-04-30', '%Y-%m-%d'))
        task_3 = Task(name="Test 3", priority_id=3, owner_id=3, due_date=datetime.strptime('2019-04-30', '%Y-%m-%d'))
        task_4 = Task(name="Test 4", priority_id=4, owner_id=2, due_date=datetime.strptime('2019-04-30', '%Y-%m-%d'))
        db.session.add(task_1)
        db.session.add(task_2)
        db.session.add(task_3)
        db.session.add(task_4)

        # commit!
        db.session.commit()
