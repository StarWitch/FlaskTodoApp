from flask import jsonify, request
from sqlalchemy import inspect
from datetime import datetime
from sqlalchemy import func
from todo.models import Task, Priority, Owner
from todo.api import api_blueprint

# primary index that routes most things
@api_blueprint.route('/', methods=['GET'])
@api_blueprint.route('/<int:single>', methods=['GET'])
def index(single=None):
    output = []
    if single:
        task = Task.query.get(single)
        if check_exists(task):
            output = [task.serialized_output()]
    else:
        tasks = Task.query.all()
        if check_exists(tasks):
            output = [task.serialized_output() for task in tasks]

    return jsonify({'tasks' : output}), 200

# add function
@api_blueprint.route('/', methods = ['POST'])
def add():
    if not request.json:
        return jsonify({ 'error' : 'must specify task name, date, priority, and owner' }), 500

    new_task = Task()
    new_task.name = request.json['name']
    new_task.due_date = datetime.strptime(request.json['due_date'], "%Y-%m-%d")
    new_task.priority_id = request.json['priority_id']
    new_task.owner_id = request.json['owner_id']
    new_task.save()

    return jsonify({'task' : new_task.serialized_output() }), 201

# edit function
@api_blueprint.route('/<int:id>', methods = ['PUT'])
def edit(id=None):
    if not request.json or not id:
        return jsonify({'error' : 'must specify id, as well as details to change'}), 500

    task = Task.query.get(id)
    if not check_exists(task):
        return jsonify({'error' : 'id not found'}), 500
    task.name = request.json['name']
    task.due_date = datetime.strptime(request.json['due_date'], "%Y-%m-%d")
    task.priority_id = request.json['priority_id']
    task.owner_id = request.json['owner_id']
    task.save()

    return jsonify({'task' : task.serialized_output() }), 200

# delete function
@api_blueprint.route('/<int:id>', methods = ['DELETE'])
def delete(id=None):
    if not id:
        return jsonify({'error' : 'must specify id to delete'}), 500

    task = Task.query.get(id)
    if not check_exists(task):
        return jsonify({'error' : 'id not found'}), 500
    task.delete()

    return jsonify({'result' : True}), 200

def check_exists(task_obj):
    if not task_obj:
        return False
    return True

