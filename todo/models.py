from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from todo import db
from datetime import datetime 
class Task(db.Model):
    '''
        Task Class

        name = description of task
        due_date = datetime object for when it's due
        priority / priority_id = foreignkey to Priority class
        owner / owner_id = foreignkey to the Owner class

    '''
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False, nullable=False)
    due_date = db.Column(db.Date, unique=False, nullable=True)
    priority_id = db.Column(db.Integer, db.ForeignKey('priority.id'), nullable=False)
    priority = db.relationship("Priority", foreign_keys=priority_id)
    owner_id = db.Column(db.Integer, db.ForeignKey('owner.id'), nullable=False)
    owner = db.relationship("Owner", foreign_keys=owner_id)

    def __repr__(self):
        return("<Task - ID: {0}, Name: {1}, Priority: {2}, Date: {3}, Owner: {4}>".format(self.id, self.name, self.priority, self.due_date, self.owner))

    def save(self):
        # helper function to save things with an obj.save() command
        db.session.add(self)
        db.session.commit()

    def delete(self):
        # same as above, but for deleting
        db.session.delete(self)
        db.session.commit()

    def serialized_output(self, human_readable=False):
        return {
                    'id' : self.id,
                    'name' : self.name,
                    'due_date' : datetime.strftime(self.due_date, "%Y-%m-%d"),
                    ('priority' if human_readable else 'priority_id') : (self.priority.name if human_readable else self.priority.id),
                    ('owner' if human_readable else 'owner_id') : (self.owner.name if human_readable else self.owner.id)
                }


class Priority(db.Model):
    '''
        Priority Class

        name = priority level (Low/Medium/High/Unassigned, etc)
        colour = colour that templates can use to assign colours to table fields (in hex, i.e: #FFFFF)

    '''
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    colour = db.Column(db.String(80))

    def __repr__(self):
        return("<Priority - ID: {0}, Name: {1}, Colour: {2}>".format(self.id, self.name, self.colour))

class Owner(db.Model):
    '''
        Owner Class

        name = name of owner (i.e: Alice, Bob, etc, or Unassigneed)
    '''
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))

    def __repr__(self):
        return("<Owner - ID: {0}, Name: {1}>".format(self.id, self.name))
