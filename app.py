import os
from todo import create_app, seed_db
# start app with default settings - assume production
app = create_app('prod.cfg')
if 'FLASK_SEED_DB' in os.environ:
    seed_db(app)
